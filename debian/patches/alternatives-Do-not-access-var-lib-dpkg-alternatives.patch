From: Benjamin Drung <benjamin.drung@ionos.com>
Date: Tue, 12 Oct 2021 14:43:31 +0200
Subject: alternatives: Do not access /var/lib/dpkg/alternatives directly
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

salt contains modules, which directly access the dpkg internal database,
instead of using one of the public interfaces provided by dpkg. This is
a problem for several reasons, because even though the layout and format
of the dpkg database is administrator friendly, and it is expected that
those might need to mess with it, in case of emergency, this “interface”
does not extend to other programs besides the dpkg suite of tools. The
admindir can also be configured differently at dpkg build or run-time.
And finally, the contents and its format, will be changing in the near
future.

So use something like:

```
update-alternatives --query $name
```

to query information about alternatives in a machine readable format.

Also add tests cases for `alternatives.show_links` using real data from
the distributions.

Bug: https://github.com/saltstack/salt/issues/52605
Bug-Debian: https://bugs.debian.org/944970
Forwarded: https://github.com/saltstack/salt/pull/58745
Signed-off-by: Benjamin Drung <benjamin.drung@ionos.com>
---
 changelog/58745.changed                         |  1 +
 salt/modules/alternatives.py                    | 55 ++++++++++++-------
 tests/pytests/unit/modules/test_alternatives.py | 73 ++++++++++++++++++++++++-
 3 files changed, 108 insertions(+), 21 deletions(-)
 create mode 100644 changelog/58745.changed

diff --git a/changelog/58745.changed b/changelog/58745.changed
new file mode 100644
index 0000000..969628d
--- /dev/null
+++ b/changelog/58745.changed
@@ -0,0 +1 @@
+alternatives: Do not access /var/lib/dpkg/alternatives directly
diff --git a/salt/modules/alternatives.py b/salt/modules/alternatives.py
index 75f09db..64df8d7 100644
--- a/salt/modules/alternatives.py
+++ b/salt/modules/alternatives.py
@@ -57,6 +57,37 @@ def display(name):
     return out["stdout"]
 
 
+def _read_alternative_link_directly(name, path):
+    try:
+        with salt.utils.files.fopen(os.path.join(path, name), "rb") as r_file:
+            contents = salt.utils.stringutils.to_unicode(r_file.read())
+            return contents.splitlines(True)[1].rstrip("\n")
+    except OSError:
+        log.error("alternatives: %s does not exist", name)
+    except (OSError, IndexError) as exc:  # pylint: disable=duplicate-except
+        log.error(
+            "alternatives: unable to get master link for %s. Exception: %s",
+            name,
+            exc,
+        )
+
+    return False
+
+
+def _read_alternative_link_with_command(name):
+    cmd = [_get_cmd(), "--query", name]
+    out = __salt__["cmd.run_all"](cmd, python_shell=False, ignore_retcode=True)
+    if out["retcode"] > 0 and out["stderr"] != "":
+        return False
+
+    first_block = out["stdout"].split("\n\n", 1)[0]
+    for line in first_block.split("\n"):
+        if line.startswith("Link:"):
+            return line.split(":", 1)[1].strip()
+
+    return False
+
+
 def show_link(name):
     """
     Display master link for the alternative
@@ -71,28 +102,12 @@ def show_link(name):
     """
 
     if __grains__["os_family"] == "RedHat":
-        path = "/var/lib/"
+        return _read_alternative_link_directly(name, "/var/lib/alternatives")
     elif __grains__["os_family"] == "Suse":
-        path = "/var/lib/rpm/"
+        return _read_alternative_link_directly(name, "/var/lib/rpm/alternatives")
     else:
-        path = "/var/lib/dpkg/"
-
-    path += "alternatives/{}".format(name)
-
-    try:
-        with salt.utils.files.fopen(path, "rb") as r_file:
-            contents = salt.utils.stringutils.to_unicode(r_file.read())
-            return contents.splitlines(True)[1].rstrip("\n")
-    except OSError:
-        log.error("alternatives: %s does not exist", name)
-    except (OSError, IndexError) as exc:  # pylint: disable=duplicate-except
-        log.error(
-            "alternatives: unable to get master link for %s. Exception: %s",
-            name,
-            exc,
-        )
-
-    return False
+        # Debian based systems
+        return _read_alternative_link_with_command(name)
 
 
 def show_current(name):
diff --git a/tests/pytests/unit/modules/test_alternatives.py b/tests/pytests/unit/modules/test_alternatives.py
index 6443013..6b76c74 100644
--- a/tests/pytests/unit/modules/test_alternatives.py
+++ b/tests/pytests/unit/modules/test_alternatives.py
@@ -1,7 +1,7 @@
 import pytest
 import salt.modules.alternatives as alternatives
 from tests.support.helpers import TstSuiteLoggingHandler
-from tests.support.mock import MagicMock, patch
+from tests.support.mock import MagicMock, mock_open, patch
 
 
 @pytest.fixture
@@ -175,3 +175,74 @@ def test_remove():
                 ["alternatives", "--remove", "better-world", "/usr/bin/better-world"],
                 python_shell=False,
             )
+
+
+ALTERNATIVE_QUERY_EDITOR = """\
+Name: editor
+Link: /usr/bin/editor
+Slaves:
+ editor.1.gz /usr/share/man/man1/editor.1.gz
+ editor.da.1.gz /usr/share/man/da/man1/editor.1.gz
+ editor.de.1.gz /usr/share/man/de/man1/editor.1.gz
+ editor.fr.1.gz /usr/share/man/fr/man1/editor.1.gz
+ editor.it.1.gz /usr/share/man/it/man1/editor.1.gz
+ editor.ja.1.gz /usr/share/man/ja/man1/editor.1.gz
+ editor.pl.1.gz /usr/share/man/pl/man1/editor.1.gz
+ editor.ru.1.gz /usr/share/man/ru/man1/editor.1.gz
+Status: manual
+Best: /bin/nano
+Value: /usr/bin/vim.basic
+
+Alternative: /bin/nano
+Priority: 40
+Slaves:
+ editor.1.gz /usr/share/man/man1/nano.1.gz
+
+Alternative: /usr/bin/mcedit
+Priority: 25
+Slaves:
+ editor.1.gz /usr/share/man/man1/mcedit.1.gz
+
+Alternative: /usr/bin/vim.basic
+Priority: 30
+Slaves:
+ editor.1.gz /usr/share/man/man1/vim.1.gz
+ editor.da.1.gz /usr/share/man/da/man1/vim.1.gz
+ editor.de.1.gz /usr/share/man/de/man1/vim.1.gz
+ editor.fr.1.gz /usr/share/man/fr/man1/vim.1.gz
+ editor.it.1.gz /usr/share/man/it/man1/vim.1.gz
+ editor.ja.1.gz /usr/share/man/ja/man1/vim.1.gz
+ editor.pl.1.gz /usr/share/man/pl/man1/vim.1.gz
+ editor.ru.1.gz /usr/share/man/ru/man1/vim.1.gz
+"""
+
+
+def test_show_link_debian():
+    """Test alternatives.show_link on Debian 10."""
+    run_all_mock = MagicMock(
+        return_value={"retcode": 0, "stderr": "", "stdout": ALTERNATIVE_QUERY_EDITOR}
+    )
+    with patch.dict(alternatives.__grains__, {"os_family": "Debian"}), patch.dict(
+        alternatives.__salt__, {"cmd.run_all": run_all_mock}
+    ):
+        assert alternatives.show_link("editor") == "/usr/bin/editor"
+
+
+def test_show_link_redhat():
+    """Test alternatives.show_link on CentOS 8."""
+    ld_data = b"auto\n/usr/bin/ld\n\n/usr/bin/ld.bfd\n50\n/usr/bin/ld.gold\n30\n"
+    with patch.dict(alternatives.__grains__, {"os_family": "RedHat"}), patch(
+        "salt.utils.files.fopen",
+        mock_open(read_data={"/var/lib/alternatives/ld": ld_data}),
+    ):
+        assert alternatives.show_link("ld") == "/usr/bin/ld"
+
+
+def test_show_link_suse():
+    """Test alternatives.show_link on openSUSE Leap 42.3."""
+    ld_data = b"auto\n/usr/bin/ld\n\n/usr/bin/ld.bfd\n2\n\n"
+    with patch.dict(alternatives.__grains__, {"os_family": "Suse"}), patch(
+        "salt.utils.files.fopen",
+        mock_open(read_data={"/var/lib/rpm/alternatives/ld": ld_data}),
+    ):
+        assert alternatives.show_link("ld") == "/usr/bin/ld"
