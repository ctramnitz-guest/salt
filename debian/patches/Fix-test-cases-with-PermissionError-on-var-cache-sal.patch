From: Benjamin Drung <benjamin.drung@ionos.com>
Date: Wed, 22 Dec 2021 10:38:31 +0100
Subject: Fix test cases with PermissionError on /var/cache/salt

When running the test cases without root permission, some test cases fail:

```
$ python3 -m pytest -ra tests/pytests/unit/state/test_state_compiler.py tests/pytests/unit/test_minion.py
[...]
FAILED tests/pytests/unit/state/test_state_compiler.py::test_render_requisite_require_disabled - PermissionError: [Errno 13] Permission denied: '/var/cache/salt'
FAILED tests/pytests/unit/state/test_state_compiler.py::test_render_requisite_require_in_disabled - PermissionError: [Errno 13] Permission denied: '/var/cache/salt'
FAILED tests/pytests/unit/test_minion.py::test_minion_module_refresh - PermissionError: [Errno 13] Permission denied: '/var/cache/salt'
FAILED tests/pytests/unit/test_minion.py::test_minion_module_refresh_beacons_refresh - PermissionError: [Errno 13] Permission denied: '/var/cache/salt'
```

Fix these test cases by using a temporary directory as cache directory.

Forwarded: https://github.com/saltstack/salt/pull/61158
Signed-off-by: Benjamin Drung <benjamin.drung@ionos.com>
---
 tests/pytests/unit/state/test_state_compiler.py | 6 ++++--
 tests/pytests/unit/test_minion.py               | 6 ++++--
 2 files changed, 8 insertions(+), 4 deletions(-)

diff --git a/tests/pytests/unit/state/test_state_compiler.py b/tests/pytests/unit/state/test_state_compiler.py
index fc43cf1..73f6336 100644
--- a/tests/pytests/unit/state/test_state_compiler.py
+++ b/tests/pytests/unit/state/test_state_compiler.py
@@ -679,7 +679,7 @@ def test_verify_retry_parsing():
             assert set(expected_result).issubset(set(state_obj.call(low_data)))
 
 
-def test_render_requisite_require_disabled():
+def test_render_requisite_require_disabled(tmp_path):
     """
     Test that the state compiler correctly deliver a rendering
     exception when a requisite cannot be resolved
@@ -710,6 +710,7 @@ def test_render_requisite_require_disabled():
         }
 
         minion_opts = salt.config.DEFAULT_MINION_OPTS.copy()
+        minion_opts["cachedir"] = str(tmp_path)
         minion_opts["disabled_requisites"] = ["require"]
         state_obj = salt.state.State(minion_opts)
         ret = state_obj.call_high(high_data)
@@ -719,7 +720,7 @@ def test_render_requisite_require_disabled():
         assert run_num == 0
 
 
-def test_render_requisite_require_in_disabled():
+def test_render_requisite_require_in_disabled(tmp_path):
     """
     Test that the state compiler correctly deliver a rendering
     exception when a requisite cannot be resolved
@@ -755,6 +756,7 @@ def test_render_requisite_require_in_disabled():
         }
 
         minion_opts = salt.config.DEFAULT_MINION_OPTS.copy()
+        minion_opts["cachedir"] = str(tmp_path)
         minion_opts["disabled_requisites"] = ["require_in"]
         state_obj = salt.state.State(minion_opts)
         ret = state_obj.call_high(high_data)
diff --git a/tests/pytests/unit/test_minion.py b/tests/pytests/unit/test_minion.py
index 985ec99..a265dfa 100644
--- a/tests/pytests/unit/test_minion.py
+++ b/tests/pytests/unit/test_minion.py
@@ -493,7 +493,7 @@ def test_scheduler_before_connect():
             minion.destroy()
 
 
-def test_minion_module_refresh():
+def test_minion_module_refresh(tmp_path):
     """
     Tests that the 'module_refresh' just return in case there is no 'schedule'
     because destroy method was already called.
@@ -507,6 +507,7 @@ def test_minion_module_refresh():
     ):
         try:
             mock_opts = salt.config.DEFAULT_MINION_OPTS.copy()
+            mock_opts["cachedir"] = str(tmp_path)
             minion = salt.minion.Minion(
                 mock_opts,
                 io_loop=salt.ext.tornado.ioloop.IOLoop(),
@@ -520,7 +521,7 @@ def test_minion_module_refresh():
             minion.destroy()
 
 
-def test_minion_module_refresh_beacons_refresh():
+def test_minion_module_refresh_beacons_refresh(tmp_path):
     """
     Tests that 'module_refresh' calls beacons_refresh and that the
     minion object has a beacons attribute with beacons.
@@ -534,6 +535,7 @@ def test_minion_module_refresh_beacons_refresh():
     ):
         try:
             mock_opts = salt.config.DEFAULT_MINION_OPTS.copy()
+            mock_opts["cachedir"] = str(tmp_path)
             minion = salt.minion.Minion(
                 mock_opts,
                 io_loop=salt.ext.tornado.ioloop.IOLoop(),
